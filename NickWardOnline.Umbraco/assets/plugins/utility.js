﻿var revapi22;
$(document).ready(function () {
    if ($("#rev_slider_22_1").revolution == undefined) {
        window.revslider_showDoubleJqueryError("#rev_slider_22_1");
    } else {
        revapi22 = window.jQuery("#rev_slider_22_1").show().revolution({
            sliderType: "carousel",
            jsFileLocation: window.plugin_path + "js/slider-revolution-v5/",
            sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "on",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                arrows: {
                    style: "hesperiden",
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 778,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    }
                }
                ,
                thumbnails: {
                    style: "gyges",
                    enable: true,
                    width: 80,
                    height: 80,
                    min_width: 80,
                    wrapper_padding: 20,
                    wrapper_color: "#222222",
                    wrapper_opacity: "1",
                    tmp: '<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
                    visibleAmount: 10,
                    hide_onmobile: false,
                    hide_onleave: false,
                    direction: "vertical",
                    span: true,
                    position: "outer-left",
                    space: 10,
                    h_align: "left",
                    v_align: "top",
                    h_offset: 0,
                    v_offset: 0
                }
            },
            carousel: {
                border_radius: "20px",
                padding_top: "50",
                padding_bottom: "50",
                maxRotation: 35,
                vary_rotation: "on",
                minScale: 25,
                vary_scale: "on",
                horizontal_align: "center",
                vertical_align: "center",
                fadeout: "on",
                vary_fade: "on",
                maxVisibleItems: 7,
                infinity: "on",
                space: 30,
                stretch: "off"
            },
            responsiveLevels: [1240, 1240, 778, 480],
            gridwidth: [640, 640, 480, 320],
            gridheight: [640, 640, 480, 320],
            lazyType: "smart",
            shadow: 0,
            spinner: "spinner3",
            stopLoop: "on",
            stopAfterLoops: 0,
            stopAtSlide: 1,
            shuffle: "off",
            autoHeight: "off",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }
});